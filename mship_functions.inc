<?php

/**
 * @file
 * All module callback and helper functions in here
 */

/**
 * callback menu function: _mship_list_all()
 */
function _mship_list_all() {
  global $user;
  
  _mship_debug('_mship_list_all()');

  // initialized variables
  $table_rows = array();
  
  $table_header = array(
    array('data' => t('Membership number'), 'field' => 'mnumber'),
    array('data' => t('Surname'), 'field' => 'surname'),
    array('data' => t('Firstname')),
    array('data' => t('User')),
    array('data' => t('Section'))
  );

  /*
   * we need to take into account here the administrators "permissions level"
   * and construct the SQL accordingly
   */
  if (user_access('mship administer') || user_access('mship view all data')) {
    $sql = 'SELECT nm.nid, nm.mship_uid_link, nm.mnumber, nm.surname, nm.firstname '.
           'FROM {node_mship} nm ';
  }
  else if (user_access('mship sub administer') || user_access('mship sub view data')) {
    if (!$sql = _mship_sub_access_node_sql_list()) {
      drupal_access_denied();
    }
  }
  
  $tablesort = tablesort_sql($table_header);
  $result = pager_query($sql . $tablesort, variable_get('mship_num_in_list', 20));       
         
  while ($record = db_fetch_array($result)) {
    
    // if this a system user?
    if ((int)$record['mship_uid_link']) {
      $local_user = user_load(array('uid' => $record['mship_uid_link']));
      $system_user = l($local_user->name, 'user/'.$local_user->uid, array(), NULL, NULL, FALSE, TRUE);
    }
    else {
      $system_user = '';
    }
    
    // extract if in any taxonomy terms and add them here to go in the "sections" col of the table
    $terms = ''; $once = 0; 
    $vocabs = variable_get('mship_assoc_vocab_vids', array());
    if (count($vocabs)) {
      $sql = "SELECT * FROM {vocabulary} WHERE vid in (%s)";
      $vocabs_result = db_query($sql, implode(',', $vocabs));
      while($vocab_row = db_fetch_array($vocabs_result)) {
        $sql = 'SELECT td.name, td.tid '.
               'FROM {term_data} td '.
               'INNER JOIN {term_node} tn ON tn.tid = td.tid '.
               'WHERE tn.nid = %d AND td.vid = %d';
        $term_result = db_query($sql, $record['nid'], $vocab_row['vid']);
        while($term_row = db_fetch_array($term_result)) {
          $record['term_rows'][$term_row['tid']] = $term_row; // saved raw data for future theme function
          if ($once++) { $terms .= ', '; }
          $terms .= l($term_row['name'], 'taxonomy/term/'.$term_row['tid'], array(), NULL, NULL, FALSE, TRUE);
        }
      }
    }
   
    // table rows
    $table_rows[] = array(
      'data' => array(
        // table cells
        l(check_plain($record['mnumber']), 'node/'.$record['nid'], array(), NULL, NULL, FALSE, TRUE),
        check_plain($record['surname']),
        check_plain($record['firstname']),
        $system_user,
        $terms
      ),
      'class' => ''
    ); 
    
    $data['raw']['records'][$record['nid']] = $record; // saved raw data for future theme function
  }
  
  $data['headers'] = $table_header;
  $data['rows'] = $table_rows;
  
  return theme('mship_list_all', $data);
}

/**
 * helper function: _mship_node_by_uid()
 *
 * @param int uid  Find a node_mship record for a given uid
 */
function _mship_node_by_uid($uid = FALSE) {
  global $user;
  static $cache_object = array();

  if (!$uid) {
    $uid = $user->uid;
  }
  
  if (isset($cache_object[$uid]) && is_object($cache_object[$uid])) {
    return $cache_object[$uid];
  }
  
  $sql = "SELECT * FROM {node_mship} WHERE mship_uid_link = %d";
  $cache_object[$uid] = db_fetch_object(db_query($sql, $uid));
  return $cache_object[$uid];
}

/**
 * helper function: _mship_get_tids_by_nid()
 *
 * @param int nid
 */
function _mship_get_tids_by_nid($nid) {
  static $cached_r = FALSE;

  $r = array();

  if ($cached_r) {
    return $cached_r; // return a cached array
  }
  
  $local_vocab = variable_get('mship_sub_admin_vocab_vid', 0);
  if (!$local_vocab) {
    return $r; // return an empty array
  }
  
  $sql = "SELECT tn.tid ".
         "FROM {term_node} tn ".
         "INNER JOIN {term_data} td ON td.tid = tn.tid ".
         "WHERE td.vid = %d AND nid = %d";
  $result = db_query($sql, (int)$local_vocab, (int)$nid);
  while($row = db_fetch_array($result)) {
    $r[$row['tid']] = $row['tid'];  
  }
  
  if (count($r) > 0) {
    foreach($r as $rterm) {
      if (is_numeric($rterm)) {
        $x = taxonomy_get_tree((int)$local_vocab, $rterm);
        if (is_array($x)) {
          foreach($x as $extra_term) {
            $r[$extra_term->tid] = $extra_term->tid;
          }
        }
      }
    }
  }
  
  $cached_r = $r;
  return $r;
}

/**
 * helper function: _mship_sub_access_node_sql_list
 *
 * This function returns SQL code that can aquire a list
 * of nids based on the current user. The access control
 * is simply "select nids based on term_data that the
 * current user is in".
 *
 * The reason we return SQL code rather than the list itself
 * is that we may want to adjust the SQL with tablesort_sql()
 * and pager_query() functions at a later stage. So, just return
 * the dynamic SQL for the given user.
 *
 * We cache the SQL on a per page basis so as not to make too many 
 * db calls as this could be an often called function on a per page
 * basis.
 */
function _mship_sub_access_node_sql_list($uid = FALSE, $fields = FALSE) {
  global $user;
  static $cache_object = array();
  
  // if 'mship_sub_admin_vocab_vid' is not set don't
  // waste time as this will return invalid SQL code
  if (!variable_get('mship_sub_admin_vocab_vid', 0)) {
    return FALSE;
  }
  
  // default UID is current user
  if (!$uid) {
    $uid = $user->uid;
  }  

  if (isset($cache_object[$uid])) {
    return $cache_object[$uid];
  }
  
  // standard default fields in the "SELECT" statement
  if (!$fields) {
    $fields = 'nm.nid, nm.mship_uid_link, nm.mnumber, nm.surname, nm.firstname';
  }

  // for this to work, the current user must be already bound to a node_mship
  // record by way of the mship_uid_link field. If that's not the case then
  // we cannot proceed
  if (!$user_node_mship = _mship_node_by_uid($uid)) {
     return FALSE;
  }
    
  // get a list of taxonomy IDs that the user's own node record is in
  if (!$result = db_query('SELECT tid FROM {term_node} WHERE nid = %d', $user_node_mship->nid)) {
    return FALSE;
  }
  while ($term_row = db_fetch_array($result)) {
    $user_terms[$term_row['tid']] = $term_row['tid'];
  }
  
  // if not in any terms then cannot construct dynamic SQL
  if (!count($user_terms)) {
    return FALSE;
  }

  if (count($user_terms) > 0) {
    foreach($user_terms as $rterm) {
      if (is_numeric($rterm)) {
        $x = taxonomy_get_tree((int)variable_get('mship_sub_admin_vocab_vid', 0), $rterm);
        if (is_array($x)) {
          foreach($x as $extra_term) {
            $user_terms[$extra_term->tid] = $extra_term->tid;
          }
        }
      }
    }
  }
    
  // construct the dynamic SQL that gets nids based on the users term settings
  $sql = 'SELECT DISTINCT ' . $fields . ' '.
         'FROM {node_mship} nm '.
         'LEFT JOIN {term_node} tn ON tn.nid = nm.nid '.
         'LEFT JOIN {term_data} td ON td.tid = tn.tid '.
         'WHERE tn.tid IN (' . implode(',', $user_terms) .')'.
         ' AND td.vid = '. (int)variable_get('mship_sub_admin_vocab_vid', 0);

  // cache and return
  $cache_object[$uid] = $sql;         
  return $sql;
} 

/**
 * helper function: _mship_access()
 */
function _mship_access($nid, $uid = FALSE) {
  global $user;
  static $cache_array;
  
  _mship_debug("_mship_access($nid, ".(int)$uid.")");
  
  if (user_access('mship administer') || user_access('mship view all data')) {
    return TRUE;
  }
  
  if (!$uid) {
    $uid = $user->uid;
  }
  
  if (!isset($cache_array[$uid])) {
    if (!($sql = _mship_sub_access_node_sql_list($uid))) {
      return FALSE;
    }
    $result = db_query($sql);
    if (!$result) {
      return FALSE;
    }
    while ($row = db_fetch_array($result)) {
      $cache_array[$uid][$row['nid']] = $row['nid'];
    }
  }
  
  if (in_array($nid, $cache_array[$uid])) {
    return TRUE;
  }
  
  return FALSE;
}

/**
 * callback function: general settings
 */
function _mship_settings() {

  $form = array();

  /* 
   * Present a select box of availabe vocabularies that a site admin
   * should select for apllying  "sub administartion delegation" perms
   * via the new "sub administer mship" permissions
   */
  $vocabs = taxonomy_get_vocabularies();
  $vocab_options[0] = t('none selected');
  foreach($vocabs as $vocab) {
    $vocab_options[$vocab->vid] = $vocab->name;
  }
  $desc = t('Use this to select which taxonomy vocabulary should be used '.
            'to apply sub administration restrictions.');
  $form['mship_sub_admin_vocab_vid'] = array(
    '#type' => 'select',
    '#title' => t('Sub administration based on vocabulary'),
    '#default_value' => variable_get('mship_sub_admin_vocab_vid', 0),
    '#options' => $vocab_options,
    '#description' => $desc,
    '#disabled' => count($vocabs) ? FALSE : TRUE,
    '#multiple' => FALSE
  );

  /* 
   * Present a select box of availabe vocabularies that a site admin
   * should select for applying  "subscription" based systems
   */
  $desc = t('Use this to select which taxonomy vocabulary should be used '.
            'to apply sub administration restrictions.');
  $form['mship_subscription_vocab_vid'] = array(
    '#type' => 'select',
    '#title' => t('Subscription/fees system based on vocabulary'),
    '#default_value' => variable_get('mship_subscription_vocab_vid', 0),
    '#options' => $vocab_options,
    '#description' => $desc,
    '#disabled' => count($vocabs) ? FALSE : TRUE,
    '#multiple' => FALSE
  );

  /* 
   * Present a multiple select box of availabe vocabularies 
   * that are "tied" to the Membership system for display
   * purposes
   */
  $desc = t('Use this to select which taxonomy vocabularies should be associated '.
            'with the membership management system.');
  $form['mship_assoc_vocab_vids'] = array(
    '#type' => 'select',
    '#title' => t('Associate these vocabularies with the membership system'),
    '#default_value' => variable_get('mship_assoc_vocab_vids', array()),
    '#options' => $vocab_options,
    '#description' => $desc,
    '#disabled' => count($vocabs) ? FALSE : TRUE,
    '#multiple' => TRUE
  );

  
  return system_settings_form($form);
}

/**
 * helper function: _mship_record_update()
 *
 * Note, this function can be called in multiple context:
 *
 * As "node_save" in which case the calling context is saving the 
 * the data as a node. In this case, $nid will be non-zero and the
 * passed in data is an "object" type.
 *
 * As "user_save" in which case the calling context is the "own user"
 * saving the data. In this case, $nid will be zero and we should use
 * the global $user->uid to do the save. Also note, in this case the
 * passed in data is a true array and not an object. Thanks Drupal for
 * the consistent use of storage containers! (not) 
 */
function _mship_record_update($edit, $nid = 0, $uid = 0) {
  global $user;
  
  _mship_debug('mship_update('.$user->uid.')');
  
  // Cope with Drupal's mish/mash piss poor usage of storage containers
  $fields = (is_object($edit)) ? (array)$edit : $edit;

  if ($uid == 0) {
    $uid = $user->uid;
  }

  switch ($nid) {
    case 0:
      $where = 'WHERE mship_uid_link = %d';
      $where_value = $uid;
      $was = db_fetch_object(db_query("SELECT * FROM {node_mship} WHERE mship_uid_link = %d", $uid));
      break;
    default:
      $where = 'WHERE nid = %d';
      $where_value = $nid;
      $was = db_fetch_object(db_query("SELECT * FROM {node_mship} WHERE nid = %d", $nid));
      break;
  }
      
  // maintain original link unless administrator has specified a new system user link  
  $local_user_uid = $was->mship_uid_link;
  if (isset($fields['user_link']) && strlen($fields['user_link'])) {
    if($local_user = user_load(array('name' => $fields['user_link']))) {
      $local_user_uid = $local_user->uid;
    }
  }
  
  // Only the admin forms have this form element set. So, if it's
  // missing (as is the case when linked users edit their own data)
  // then we use the "was" version to ensure it get's included in
  // the update
  if (!isset($fields['mnumber'])) {
    $fields['mnumber'] = $was->mnumber;
  }
  
  $sql = 
    'UPDATE {node_mship} SET '.
    ' mnumber = "%s",'.
    ' mship_uid_link = %d,'.
    ' surname = "%s",'.
    ' firstname= "%s",'.
    ' address_line1 = "%s",'.
    ' address_line2 = "%s",'.
    ' city = "%s",'.
    ' postcode = "%s",'.
    ' tel_number = "%s", '.
    ' dob = "%s", '.
    ' mship_email = "%s" '.
    $where;
  
  db_query($sql,
    $fields['mnumber'], 
    $local_user_uid,
    $fields['surname'], 
    $fields['firstname'],
    $fields['address_line1'],
    $fields['address_line2'],
    $fields['city'],
    $fields['postcode'],
    $fields['tel_number'],
    $fields['dob'],
    $fields['mship_email'],
    $where_value
  );
  
  $auditlog = ''; $is_update = 0; $once = 0;
  if (isset($fields['auditlog']) && strlen($fields['auditlog'])) {
    $auditlog = $fields['auditlog'] . "\n"; 
    $is_update++;
  }
    
  foreach((array)$was as $k => $v) {
    if (isset($fields[$k]) && $v != $fields[$k]) {
      if ($once++) {
        $auditlog .= "\n";
      }
      $auditlog .= "Field '$k' was '$v' and is now '" . $fields[$k] . "'";
      $is_update++;
    }
  }

  if ($is_update) {
    db_query('INSERT INTO {node_mship_audit} (nid, uid, ondate, auditlog) VALUES (%d, %d, %d, "%s")',
      $was->nid, $user->uid, time(), $auditlog); 
  }
}

/**
 * Helper function: _mship_user__categories()
 * Supports hook mship_user()
 *
 * @see mship_user()
 */
function _mship_user__categories($edit, $user, $category) {

  $data = array();
  
  // this information is only available to "user administrators"
  // or system users who are assigned permissions to "edit own"
  if (user_access('mship administer') || user_access('mship edit own data')) { 
    $data[] = array(
      'name' => 'Membership',
      'title' => t('Membership'),
      'weight' => 10
    );
  }
   
  return $data;
}

/**
 * Helper function: _mship_get_audit($nid)
 *
 * Used to aquire an array of elements that comprise the
 * audit (edit) history for this record.
 */
function _mship_get_audit($nid) {
  
  $items = array();
  
  $r = db_query('SELECT * FROM {node_mship_audit} WHERE nid = %d ORDER BY ondate DESC', $nid);
  if ($r) {
    while($row = db_fetch_object($r)) {
      $row->user  = user_load(array('uid' => $row->uid));
      $items[] = $row;
    }
  }
  
  return $items;
}

/**
 * Helper function: _mship_get_vid()
 *
 * Returns the vocabulary id for mship navigation. Note, this
 * code was shamelessly ripped from forum.module (and I notice
 * project.module does the same which is where I first found 
 * it, a lot of ripping going on! ;)
 */
function _mship_get_vid() {
  $vid = variable_get('mship_nav_vocabulary', '');
  if (empty($vid)) {
    // Check to see if a mship vocabulary exists
    $vid = db_result(db_query("SELECT vid FROM {vocabulary} WHERE module = '%s'", 'mship'));
    if (!$vid) {
      // Create the mship vocabulary. 
      $edit = array(
        'name' => 'Membership', 
        'multiple' => 0, 
        'required' => 1, 
        'hierarchy' => 1, 
        'relations' => 0, 
        'module' => 'mship', 
        'weight' => -10, 
        'nodes' => array('mship' => 1));
      taxonomy_save_vocabulary($edit);
      $vid = $edit['vid'];
    }
    variable_set('mship_nav_vocabulary', $vid);
  }

  return $vid;
}

/**
 * Helper function: _mship_form()
 *
 * Returns a Forms API array that comprises the input
 * form for our additional node extension. Note, this 
 * is a generic form. It's called by the node editor
 * and also by the user edit system (for when a link
 * to a system user exists and perm to "edit own") so
 * be aware that changes to this form need testing in 
 * all places input is made.
 */
function _mship_form(&$node, &$param) {
  _mship_debug('_mship_form($nid = '.$node->nid.')');
  
  $form = array();

  $form['mship_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enter membership data'),
    '#weight' => -100
  );     
  
  // Only admins can "set/change" the membership number record
  if (user_access('administer mship')) {
    $form['mship_form']['mnumber_was'] = array(
      '#type' => 'hidden',
      '#default_value' => $node->mnumber
    );
    $form['mship_form']['mnumber'] = array(
      '#type' => 'textfield',
      '#title' => t('membership number'),
      '#required' => TRUE,
      '#default_value' => $node->mnumber,
      '#size' => 12,
      '#weight' => -100
    );
  }
  else {
    $form['mship_form']['mnumber'] = array(
      '#type' => 'markup',
      '#value' => theme('mship_form_membership_number', $node->mnumber),
      '#weight' => -100
    );
  }

  $form['mship_form']['surname'] = array(
    '#type' => 'textfield',
    '#title' => t('surname'),
    '#required' => TRUE,
    '#default_value' => $node->surname,
    '#size' => 12,
    '#weight' => -99
  );
  $form['mship_form']['firstname'] = array(
    '#type' => 'textfield',
    '#title' => t('firstname'),
    '#required' => FALSE,
    '#default_value' => $node->firstname,
    '#size' => 20,
    '#weight' => -98
  );
  $form['mship_form']['address_line1'] = array(
    '#type' => 'textfield',
    '#title' => t('address, line1'),
    '#required' => FALSE,
    '#default_value' => $node->address_line1,
    '#size' => 32,
    '#weight' => -97
  );
  $form['mship_form']['address_line2'] = array(
    '#type' => 'textfield',
    '#title' => t('address, line2'),  
    '#required' => FALSE,
    '#default_value' => $node->address_line2,
    '#size' => 32,
    '#weight' => -96
  );
  $form['mship_form']['city'] = array(
    '#type' => 'textfield',
    '#title' => t('city'),
    '#required' => FALSE,
    '#default_value' => $node->city,
    '#size' => 24,
    '#weight' => -95
  );
  $form['mship_form']['postcode'] = array(         
    '#type' => 'textfield',
    '#title' => t('postcode/zipcode'),          
    '#required' => FALSE,
    '#default_value' => $node->postcode,         
    '#size' => 12,
    '#weight' => -94
  );
  $form['mship_form']['tel_number'] = array(
    '#type' => 'textfield',
    '#title' => t('telephone'),
    '#required' => FALSE,
    '#default_value' => $node->tel_number,
    '#size' => 16,
    '#weight' => -93
  );
  $form['mship_form']['mship_email'] = array(
    '#type' => 'textfield',
    '#title' => t('email'),
    '#required' => FALSE,
    '#default_value' => $node->mship_email,
    '#size' => 32,
    '#weight' => -92
  );
  $form['mship_form']['dob'] = array(
    '#type' => 'textfield',
    '#title' => t('date of birth'),
    '#required' => FALSE,
    '#default_value' => $node->dob,
    '#size' => 16,
    '#weight' => -91,
    '#description' => t('in dd/mm/yyyy format')
  );

  if (user_access('administer mship')) {
    $default_user_link = '';
    if ($node->mship_uid_link != 0) {
      $local_user = user_load(array('uid' => $node->mship_uid_link));
      $default_user_link = $local_user->name;
    }
    $form['mship_form']['user_link'] = array(
      '#type' => 'textfield',
      '#title' => t('system user link'),
      '#required' => FALSE,
      '#default_value' => $default_user_link,
      '#size' => 16,
      '#weight' => -90,
      '#autocomplete_path' => 'user/autocomplete',
      '#description' => t('Select a system user to link to this membership record')
    );
    $form['mship_form']['body_filter']['body'] = array(
      '#type' => 'textarea',
      '#title' => t('Notes'),
      '#default_value' => $node->body,
      '#required' => FALSE,
      '#weight' => -1,
      '#description' => t('General notes area for this membership record')
    );
    $form['mship_form']['body_filter']['filter'] = filter_form($node->format); 

    $form['augitlog_fliter']['auditlog'] = array(
      '#type' => 'textarea',
      '#title' => t('Audit history notes'),
      '#default_value' => '',
      '#required' => FALSE,
      '#weight' => 100,
      '#description' => t('When updating a record make a note here what changes were made and why in the "audit history notes"')
    );
    $form['auditlog_filter']['filter'] = filter_form($node->format); 
  }

  return $form;
}

function _mship_form_validate(&$edit) {
  _mship_debug("_mship_form_validate()");

  // workaround Drupal's senseless use of data containers, 
  // sometimes it's an object, sometimes it's an array
  // depending on which section we are coming from (user
  // edits are array, nodes are objects)
  $parts = (is_object($edit)) ? (array)($edit) : $edit;
  
  // mnumber (membership number) must be unique
  if ($parts['mnumber_was'] != $parts['mnumber']) {
    $result = db_query('SELECT nid FROM {node_mship} WHERE mnumber = "%s"', $parts['mnumber']);
    if (db_num_rows($result)) {
      _mship_debug(' membership number in use');
      form_set_error('mnumber', t('membership number already exists.'));
    }
  }
  
  // make sure email is at least syntax correct
  if (strlen($parts['mship_email']) && !valid_email_address($parts['mship_email'])) {
    form_set_error('mship_email', t('email address does not appear to be valid.'));
  }  
  
  return $edit;
}


function _mship_tutorial_install() {
  _mship_debug("_mship_tutorial_install()");
  include_once(drupal_get_path('module', MSHIP_MODULE_NAME) . '/mship_example.inc');
  _mship_ex_install();
}

function _mship_tutorial_uninstall() {
  _mship_debug("_mship_tutorial_uninstall()");
  include_once(drupal_get_path('module', MSHIP_MODULE_NAME) . '/mship_example.inc');
  _mship_ex_uninstall();
}





