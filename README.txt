
Membership Management System (mship)
====================================

This module creates a new node type that allows site administrators
to manage a "membership" system. It's true that this could have been
done with CCK or Flexinode but the client in my case is going to be 
pushing forwards with alot of additional "bolt ons" to this so I wanted
to be sure that the skeletal outline for these records followed my own
method. Hence the module.

This module is experimental and is a first draft until all additional
features are added at the clients request. This module is Drupal5 and
above only.

Starting Out
============
The first thing to do is unpack the mship files into the mship/ directory
in your modules/ folder. The system is self installing.

As Admin (UID==1) go to admin/settings/modules and enable the mship module.

Doing this will install the database tables required for this module. 
Additionally, a "vocabulary" wil be created called "Membership". This
vocab is bound to the mship node type. You should go to admin/content/taxonomy
and select "add terms" to that vocab. For testing, I created 3 called "Sales",
"Accounts" and "R & D". You get the idea, these terms are used to group various
membership records into categories. My test categories were company depts but
you can base the category on whatever subject you like.

Moving On, permissions
======================
The Admin user (UID==1) will always have full access to the mship system.
However, it's envisaged that management of the mship records will be delegated
to a "Membership Secretary" for example.

Go to admin/user/roles and create a roll that is intended to assign mship managers
to. For example, I created "mship administrator". Then, on the mship perm list at
admin/user/access for that role, I clicked on "mship module administer mship"

Users in this role will now have the "Create member" permissions in the "Create content"
menu item.

We'll come back to what the other three perms are for later.

Menu item
=========
If you now go to admin/build/menu you will notice "Membership List" is an available
menu item. Click this on.

Create some member records
==========================
Go to node/add/mship and you will see a form that allows you to enter basic membership
data. The fields that are here are not yet fixed, some maybe removed, others added, but
you get the idea. Most records are self expalntatory with teh exception of these:-

System user link: Membership records are just simple data records about a membership list.
These members aren't users of your site, it's just a membership system. However, people
may register to use your site. If you want to bind a membership record to a drupal user
this field allows for that. The field is an auto-complete field and takes a drupal username.

Coming back to permissions for a moment. There's one called "edit own mship data". If a linked
user (as above) happens to be in a role that has been assigned this permission, then under
the "my account" an additional "tab" will appear that will allow that user to update their
own membership record. The fields available for editing are a subset of all available fields
(common ones like surname, etc). This allows for "live" users to maintain your membership
records for themselves. Handy feature if you can get them to do it!

That's it for now. Try it and see. Like I said at the start, this module is work in progress.




