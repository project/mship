
TODO/Features

1. When new system users register try to match their email address
against any in the membership system. If a match is found and "autolink"
(an admin setting to be added) is enabled then auto link the new user
with a membership recotd.

2. Basic invite.module integration has been done. However, the module is
somewhat behind on coming to Drupal5 (despite me supplying a patch that
makes it so). The intention is for mship admins to "click a link" which
will send out an invite to join the drupal system/site with the email
being supplied by the mship module (this is part done already).

3. Take a look at workflows and actions modules and see if they can add
any benfit to the membership system.

I expect more as I'm elucidated by my client.
