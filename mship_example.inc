<?php

/*
 * @file
 * Creates a sample membership system for use with the tutorial
 * supplied with the system.
 */

define('MSHIP_INSTALL_VAR', 'mship_ex_installed_5');

/**
 *  _mship_ex_install() 
 *
 * install the sample membership database
 */
function _mship_ex_install() {
  _mship_debug("_mship_ex_install()");
  
  // don't install the example membership database more than once
  // yes, I know, why am I not using variable_get(), bloody cache
  // is why. despite trying to expire the cache it still persistently
  // returns the wrong answer after I have done a delete from the table
  $r = db_query('SELECT value FROM {variable} WHERE name = "%s"', MSHIP_INSTALL_VAR);
  $v = FALSE;
  if ($r) {
    $v = db_fetch_object($r);
    $v = unserialize($v->value);
  }
  
  if ($v) {
    drupal_set_message('Membership tutorial already installed');
    drupal_goto();
    _mship_debug(" _mship_ex_install(): early return");
    return; 
  }
  
  _mship_ex_data_structures();
  _mship_ex_install_taxonomy();
  _mship_ex_install_records();
  variable_set(MSHIP_INSTALL_VAR, 1);  
  drupal_set_message('Mship tutorial isntalled');
  drupal_goto();
}

/**
 * _mship_ex_unistall()
 */
function _mship_ex_uninstall() {

  $r = db_query('SELECT value FROM {variable} WHERE name = "%s"', MSHIP_INSTALL_VAR);
  $v = FALSE;
  if ($r) {
    $v = db_fetch_object($r);
    $v = unserialize($v->value);
  }

  if (!$v) {
    drupal_set_message('Membership tutorial not installed');
    drupal_goto();
    _mship_debug(" _mship_ex_install(): early return");
    return;   
  }
  
  $vocabs = variable_get('mship_ex_vocabs', 0);
  if (is_array($vocabs)) {
    foreach($vocabs as $vid) {
      taxonomy_del_vocabulary($vid);
    }
  }
  
  /* I expected this was teh proper way to do it 
     but caching is my worst enemy, do it the hard 
     way as below
  $r = db_query('SELECT nid FROM {node_mship}');
  if ($r) {
    while ($row = db_fetch_object($r)) {
      node_delete($row->nid);
    }
  }
  */
  
  db_query('DELETE FROM {node} WHERE nid IN (SELECT nid FROM {node_mship})');
  db_query('DELETE FROM {node_revisions} WHERE nid IN (SELECT nid FROM {node_mship})');
  db_query('DELETE FROM {node_mship}');
  
  db_query('DELETE FROM variable WHERE name LIKE "mship_%%"');
  drupal_set_message('Mship tutorial unisntalled');
  drupal_goto();
  return;
}
 
/**
 * _mship_ex_install_taxonomies()
 */
function _mship_ex_install_taxonomy() {

  foreach($GLOBALS['mship']['example']['vocabularies'] as $vocabulary_name => $edit) {
    $edit['name'] = $vocabulary_name;
    taxonomy_save_vocabulary($edit);
    $GLOBALS['mship']['example']['vocabularies'][$vocabulary_name]['vid'] = $vid = $edit['vid'];
    $vocabs[$edit['name']] = $vid;
    foreach($edit['terms'] as $term_name => $edit) {
      $edit['vid'] = $vid;
      taxonomy_save_term($edit);
      $GLOBALS['mship']['example']['vocabularies'][$vocabulary_name]['terms']['tid'] = $tid = $edit['tid'];
      $terms[$edit['name']] = $tid;
    }
  }
  
  variable_set('mship_ex_vocabs', $vocabs);
  variable_set('mship_ex_terms', $terms);
  return TRUE;
}
/**
 * _mship_ex_install_records()
 */
function _mship_ex_install_records() {

  if (!($terms = variable_get('mship_ex_terms', 0))) {
    return FALSE;
  }
  
  $mnumber = 10000;
  foreach($GLOBALS['mship']['example']['records'] as $record) {
    $mnumber++;
    $nterms = $record['terms'];
    unset($record['terms']);
    $o = new StdClass;
    $o->nid = 0;
    $o->mnumber = $mnumber;
    $o->status = 1;
    $o->surname = $record[0];
    $o->firstname = $record[1];
    $o->title = $o->surname . ', ' . $o->firstname;
    $o->body = 'Example user';
    $o->type = MSHIP_MODULE_NAME;
    $o->promote = $o->sticky = $o->comment = $o->uid = 0;
    node_save($o);
    $records[$o->nid] = $o->nid;
    $tterms = array();
    foreach($nterms as $term_name) {
      $tterms[] = $terms[$term_name];
    }
    taxonomy_node_save($o->nid, $tterms);
    unset($o);
  }
  
  return TRUE;
}

/*
 * sample data structures follow here
 */
function _mship_ex_data_structures() {
  
  
  $GLOBALS['mship']['example']['records'] = array(
    array('Baldwin', 'Martin', 'terms' => array('London', 'Member', 'Maths')),
    array('Winterbottom', 'Mike', 'terms' => array('London', 'Student', 'English')),
    array('Barker', 'Ronald', 'terms' => array('London', 'Member', 'Art')),  
    array('Baker', 'Fred', 'terms' => array('London', 'Member', 'Science')),  
    array('Rogers', 'Lucy', 'terms' => array('London', 'Member', 'History')),
    array('Donaldson', 'Keneth', 'terms' => array('London', 'Student', 'History')),
    array('Peters', 'Dave', 'terms' => array('London', 'Associate Member', 'Science')),
    array('Price', 'Amanda', 'terms' => array('London', 'Member', 'Maths')),
    array('Plank', 'Max', 'terms' => array('London', 'Fellow', 'Science')),
    array('Tamworth', 'Alec', 'terms' => array('London', 'Member', 'English')),
    array('Hewitt', 'George', 'terms' => array('London', 'Member', 'Art')),
    array('Lewis', 'Peter', 'terms' => array('London', 'Member', 'History')),
    array('Dent', 'Arthor', 'terms' => array('London', 'Member', 'Science')),
    array('Baldard', 'Ken', 'terms' => array('London', 'Member', 'Art')),
    array('Richardson', 'Stephen', 'terms' => array('London', 'Member', 'Maths')),
    array('Pike', 'Andy', 'terms' => array('London', 'Fellow', 'Science')),
    array('Falstaff', 'L', 'terms' => array('London', 'Associate Member', 'English')),
    array('Stewart', 'K', 'terms' => array('London', 'Member', 'Art')),
    array('Johnson', 'Jack', 'terms' => array('London', 'Member', 'History')),
    array('Wade', 'Emmy', 'terms' => array('London', 'Member', 'Science')),
    array('Peters', 'Jock', 'terms' => array('London', 'Member', 'Maths')),
    array('Randle', 'Derrick', 'terms' => array('London', 'Fellow', 'Science')),
    array('Shakespear', 'Unknown', 'terms' => array('London', 'Fellow', 'English')),
    array('Randle', 'Rose', 'terms' => array('London', 'Member', 'Science')),
    array('Bent', 'Carl', 'terms' => array('London', 'Member', 'Art')),
    array('Hane', 'Chris', 'terms' => array('Midlands', 'Member', 'English')),
    array('Chatum', 'Lewis', 'terms' => array('Midlands', 'Member', 'Maths')),
    array('Khan', 'Genghis', 'terms' => array('Midlands', 'Student', 'Art')),
    array('Hister', 'A', 'terms' => array('Midlands', 'Student', 'History')),
    array('Dent', 'Steve', 'terms' => array('Midlands', 'Student', 'Science')),
    array('Dewer', 'Gavin', 'terms' => array('Midlands', 'Student', 'Art')),
    array('Dewer', 'Susan', 'terms' => array('Midlands', 'Member', 'History')),
    array('Dewer', 'Susan', 'terms' => array('Northern & Yorkshire', 'Member', 'History')),
    array('Bolton', 'Steve', 'terms' => array('Northern & Yorkshire', 'Member', 'Science')),
    array('Fawcett', 'Faye', 'terms' => array('Northern & Yorkshire', 'Member', 'Art'))
  );

  // define vocabs and their terms 
  $GLOBALS['mship']['example']['vocabularies']['Membership area'] = array(
    'description' => 'Defines a geographical region of members',
    'help' => 'Defines a geographical region of members',
    'relations' => 0,
    'hierarchy' => 0,
    'multiple' => 1,
    'required' => 1,
    'tags' => 0,
    'module' => MSHIP_MODULE_NAME,
    'weight' => -10,
    'nodes' => array('mship' => 1),
    'terms' => array(
      array('name' => 'London',               'description' => '', 'weight' => 0),
      array('name' => 'South East',           'description' => '', 'weight' => 0),
      array('name' => 'South West',           'description' => '', 'weight' => 0),
      array('name' => 'Midlands',             'description' => '', 'weight' => 0),
      array('name' => 'Northern & Yorkshire', 'description' => '', 'weight' => 0),
      array('name' => 'Eastern',              'description' => '', 'weight' => 0),
      array('name' => 'Trent',                'description' => '', 'weight' => 0)
    )
  );
  $GLOBALS['mship']['example']['vocabularies']['Membership type'] = array(
    'description' => 'Defines a type of member',
    'help' => 'Defines a type of member',
    'relations' => 0,
    'hierarchy' => 0,
    'multiple' => 1,
    'required' => 1,
    'tags' => 0,
    'module' => MSHIP_MODULE_NAME,
    'weight' => -9,
    'nodes' => array('mship' => 1),
    'terms' => array(
      array('name' => 'Student',          'description' => '', 'weight' => 0),
      array('name' => 'Member',           'description' => '', 'weight' => 0),
      array('name' => 'Associate Member', 'description' => '', 'weight' => 0),
      array('name' => 'Fellow',           'description' => '', 'weight' => 0)
    )
  );
  $GLOBALS['mship']['example']['vocabularies']['Membership subject'] = array(
    'description' => "Defines a member's subject",
    'help' => "Defines a member's subject",
    'relations' => 0,
    'hierarchy' => 0,
    'multiple' => 1,
    'required' => 1,
    'tags' => 0,
    'module' => MSHIP_MODULE_NAME,
    'weight' => -8,
    'nodes' => array('mship' => 1),
    'terms' => array(
      array('name' => 'English',     'description' => '', 'weight' => 0),
      array('name' => 'Mathematics', 'description' => '', 'weight' => 0),
      array('name' => 'Science',     'description' => '', 'weight' => 0),
      array('name' => 'History',     'description' => '', 'weight' => 0),
      array('name' => 'Art',         'description' => '', 'weight' => 0)
    )
  );
}
