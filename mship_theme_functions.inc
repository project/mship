<?php

/**
 * @file
 * All module theme_functions in here
 */
 
/**
 * theme function: _mship_list_all()
 *
 * @param array $data An assoc array conating all you need to build a themed table
 *
 * ['headers'] Table header info (can pass straight to theme_table)
 * ['rows']    Table rows data (can pass straight to theme_table)
 * ['raw']['records'] This is the raw data that is used to make the above.
 *
 * the 'records' element is passed in case you want to do something 
 * funky in a theme override function other than just create a table. 
 * Otherwise, it's unused.
 */
function theme_mship_list_all($data) {
  
  /*
   * If you are trying to override this theme function and want
   * to see the raw data structure, ensure debugging is switched
   * on and then uncomment the following line. You'll then see
   * the entire data structure passed in to this theme function.
   */
  
  $output  = '<p />' . theme('table', $data['headers'], $data['rows'], array('cellpadding' => 5, 'cellspacing' => 5));
  $output .= '<p />' . theme('pager', NULL, 20, 0);
  
  return $output;
}

/**
 * theme function: _mship_display_record($node)
 *
 * This is the main theme function for displaying a node/xxx
 * for a mship record.
 */
function theme_mship_display_record($node) {

  $node->content['markup1'] = array(
    '#value' => "<table border=0>",
    '#weight' => -100000
  );
  $node->content['mnumber'] = array(
    '#value' => theme('mship_field_display', 'mnumber', 'Membership number', $node),
    '#weight' => -999
  );
  $node->content['surname'] = array(
    '#value' => theme('mship_field_display', 'surname', 'Surname', $node),
    '#weight' => -998
  );
  $node->content['firstname'] = array(
    '#value' => theme('mship_field_display', 'firstname', 'Firstname', $node),
    '#weight' => -997
  );
  $node->content['address_line1'] = array(
    '#value' => theme('mship_field_display', 'address_line1', 'Address', $node),
    '#weight' => -996
  );
  $node->content['address_line2'] = array(
    '#value' => theme('mship_field_display', 'address_line2', '&nbsp;', $node),
    '#weight' => -995
  );
  $node->content['city'] = array(
    '#value' => theme('mship_field_display', 'city', '&nbsp;', $node),
    '#weight' => -994
  );
  $node->content['postcode'] = array(
    '#value' => theme('mship_field_display', 'postcode', 'Postcode', $node),
    '#weight' => -993
  );
  $node->content['tel_number'] = array(
    '#value' => theme('mship_field_display', 'tel_number', 'Telephone', $node),
    '#weight' => -992
  );
  
  $node->content['mship_email'] = array(
    '#value' => theme('mship_field_display', 'mship_email', 'Email', $node),
    '#weight' => -991
  );

  $node->content['dob'] = array(
    '#value' => theme('mship_field_display', 'dob', 'Date of birth', $node),
    '#weight' => -990
  );

  $node->content['markup2'] = array(
    '#value' => "</table>",
    '#weight' => 100000
  );

  $node->content['body'] = array(
    '#value' => theme('mship_field_display', 'body', 'Notes:', $node),
    '#weight' => 100201
  );
  
  if (isset($node->mship_audit)) {
    $node->content['auditset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Audit history'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 100301    
    );
    foreach($node->mship_audit as $auditlog) {
      $s = l('by '. $auditlog->user->name, 'user/'.$auditlog->user->uid, array('title' => $auditlogs>auditlog));
      $s .= ' on ' . date("jS F Y", $auditlog->ondate) . ' at ' .date("H:i:s", $auditlog->ondate). '<br />' . "\n";
      if (strlen($auditlog->auditlog)) {
        $s .= "\n<blockquote><pre>".$auditlog->auditlog."</pre></blockquote>";
      }
      $node->content['auditset']['auditlog'. $auditlog->ondate] = array(
        '#type' => 'markup',
        '#value' => $s
      );
    }    
  }
  
  return $node;
}
  
/**
 * theme function: _mship_display_record_teaser($node)
 *
 * The teaser display is just "the title". This is displayed
 * by default anyhow so don't do any further formatting here
 */
function theme_mship_display_record_teaser($node) {
  return $node;
}

/**
 * theme function: _mship_field_display($field, $title, $node)
 *
 * This is used to display each record line in a record display
 */
function theme_mship_field_display($field, $title = '', $node) {
  switch ($field) {
    case 'body':
      $s = '<p><b>Notes</b><br /><pre>' . $node->$field . '</pre></p>';
      break;
    case 'mnumber':
      $s = '<tr><td align="right">' . $title . '</td><td>&nbsp;</td><td><b>' . check_plain($node->$field) . '</b></td></tr>';
      break;
    case 'mship_email':
      $s = '<tr><td align="right">' . $title . '</td><td>&nbsp;</td><td><a href="mailto:'.check_plain($node->$field). '">'.check_plain($node->$field).'</td></tr>';
      break;
    default:
      $s = '<tr><td align="right">' . $title . '</td><td>&nbsp;</td><td>' . check_plain($node->$field) . '</td></tr>';
      break;
  }
  
  return $s;
}

/**
 * theme function: _mship_form_member_number($mnumber)
 *
 * Only users with 'administer mship' permissions are
 * allowed to enter/edit the mnumber field. For all other's it's
 * displayed as fixed text. This theme function returns what
 * that fixed text should look like on the form page
 */
function theme_mship_form_membership_number($mnumber) {
  return '<br /><b><u>' . t('Membership number') . ': ' . check_plain($mnumber) . '</u></b>'; 
}

function theme_mship_display_debug_inline() {

  $s = '';  
  foreach($GLOBALS[MSHIP_MODULE_NAME]['debug'] as $lines) {
    $s .= $lines . "\n";
  }
  return $s;
}


